drop table if exists ec.user;
create table ec.user
(
    id int unsigned auto_increment primary key comment 'phone_number US AI PK',
    username     varchar(255) not null comment 'username',
    password     varchar(255) not null comment 'password'
)comment 'user table';

insert into ec.user values (null,'admin','123');
insert into ec.user values (null,'Tom','123');




create table goods
(
    goods_id         integer unsigned null,
    goods_name       varchar(255)     null,
    good_brand       varchar(255)     null,
    goods_price      varchar(255)     null,
    good_description varchar(255)     null
);

insert into ec.goods values (null,'华为畅享60','HUAWEI','1290.00','6000mAh+22.5W超级快充 4800万大底超清影像 128GB 晨曦金 鸿蒙智能手机');
insert into ec.goods values (null,'华为畅享60','HUAWEI','1290.00','6000mAh+22.5W超级快充 4800万大底超清影像 128GB 晨曦金 鸿蒙智能手机');
insert into ec.goods values (null,'华为畅享60','HUAWEI','1290.00','6000mAh+22.5W超级快充 4800万大底超清影像 128GB 晨曦金 鸿蒙智能手机');
insert into ec.goods values (null,'华为畅享60','HUAWEI','1290.00','6000mAh+22.5W超级快充 4800万大底超清影像 128GB 晨曦金 鸿蒙智能手机');
insert into ec.goods values (null,'华为畅享60','HUAWEI','1290.00','6000mAh+22.5W超级快充 4800万大底超清影像 128GB 晨曦金 鸿蒙智能手机');
insert into ec.goods values (null,'华为畅享60','HUAWEI','1290.00','6000mAh+22.5W超级快充 4800万大底超清影像 128GB 晨曦金 鸿蒙智能手机');
insert into ec.goods values (null,'华为畅享60','HUAWEI','1290.00','6000mAh+22.5W超级快充 4800万大底超清影像 128GB 晨曦金 鸿蒙智能手机');
insert into ec.goods values (null,'华为畅享60','HUAWEI','1290.00','6000mAh+22.5W超级快充 4800万大底超清影像 128GB 晨曦金 鸿蒙智能手机');
insert into ec.goods values (null,'华为畅享60','HUAWEI','1290.00','6000mAh+22.5W超级快充 4800万大底超清影像 128GB 晨曦金 鸿蒙智能手机');

# 1,华为畅享60,HUAWEI,1290.00,6000mAh+22.5W超级快充 4800万大底超清影像 128GB 晨曦金 鸿蒙智能手机
# 2,OPPOa56s,OPPO,1099.00,8GB+128GB 深海蓝 双模5G 天玑810 5000mAh大电池 200%的超级音量 5G手机
# 3,华为P50,HUAWEI,4228.00,原色双影像单元 基于鸿蒙操作系统 万象双环设计 支持66W超级快充 8GB+256GB曜金黑 华为手机
# 4,魅族18s,魅族,2699.00,8GB+128GB 渡海 5G 骁龙888+ 支持36W超充 6.2英寸2K曲面屏 6400W高清三摄光学防抖 拍照手机
# 6,小米13,小米,4599.00,徕卡光学镜头 第二代骁龙8处理器 超窄边屏幕 120Hz高刷 67W快充 12+256GB 黑色 5G手机
# 7,iPhone14ProMax,Apple,9899.00,Apple iPhone 14 Pro Max (A2896) 256GB 暗紫色 支持移动联通电信5G 双卡双待手机
