<%--
  Created by IntelliJ IDEA.
  User: zx
  Date: 2023/4/8
  Time: 16:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Edit Page</title>
</head>
<body>
<center>
  <h1>修改</h1>
  <form action="/goods/modify" method="post">
    <p><input type="hidden" name="goodsId" value="${sessionScope.good.goodsId}"></p>
    <p><input type="text" name="goodsName" value="${sessionScope.good.goodsName}" placeholder="goodsName" ></p>
    <p><input type="text" name="goodBrand" value="${sessionScope.good.goodBrand}" placeholder="goodsBrand"></p>
    <p><input type="text" name="goodsPrice" value="${sessionScope.good.goodsPrice}" placeholder="goodsPrice"></p>
    <p><input type="text" name="goodsDescription" value="${sessionScope.good.goodsDescription}" placeholder="goodsDescription"></p>
    <p><input type="submit" value="提交"></p>
  </form>



</center>

</body>
</html>
