<%--
  Created by IntelliJ IDEA.
  User: zx
  Date: 2023/4/8
  Time: 11:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Admin Page</title>
    <script>
        function del(){
            return confirm("删除这条记录吗?");
        }
    </script>
</head>
<body>
<center>
    <h1>管理界面</h1>
    <p><a href="/admin/add.jsp">添加</a></p>
    <table border="1">
        <tr>
            <th>商品ID</th>
            <th>商品名称</th>
            <th>商品品牌</th>
            <th>商品价格</th>
            <th>商品详情</th>
            <th colspan="3">操作</th>
        </tr>
        <c:forEach var="good" items="${sessionScope.goods}">
            <tr>
                <td>${good.goodsId}</td>
                <td>${good.goodsName}</td>
                <td>${good.goodBrand}</td>
                <td>${good.goodsPrice}</td>
                <td>${good.goodsDescription}</td>
                <td><a href="/goods/queryById/${good.goodsId}">修改</a></td>
                <td><a href="/goods/removeById/${good.goodsId}" onclick="return del()">删除</a></td>
            </tr>
        </c:forEach>

    </table>
    <p><a href="/goods/signOut">退出</a></p>
</center>
</body>
</html>
