<%--
  Created by IntelliJ IDEA.
  User: zx
  Date: 2023/4/8
  Time: 11:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
</head>
<body style="background: gold">
<center>
    <h1 style="background-color: crimson">欣欣商城欢迎您:${sessionScope.user.username}</h1>
    <form action="/goods/selectByNames" method="post">
        品牌名:<input type="text" name="goodBrand" placeholder="goodBrand">
        <p><input type="submit" value="提交"></p>
        <p><h1 >${requestScope.b}</h1></p>

    </form>

</center>


<center>

    <table border="1">
        <tr>
            <th>商品名称</th>
            <th>商品品牌</th>
            <th>商品价格</th>
            <th>商品详情</th>
            <th>操作</th>
        </tr>
        <c:forEach var="good" items="${sessionScope.goods}">
            <tr>
                <td>${good.goodsName}</td>
                <td>${good.goodBrand}</td>
                <td>${good.goodsPrice}</td>
                <td>${good.goodsDescription}</td>
                <td><a href="/goods/userQueryById/${good.goodsId}">查看</a></td>
            </tr>
        </c:forEach>
    </table>
</center>
<p><a href="/user/signOut">退出</a></p>

</body>
</html>
