<%--
  Created by IntelliJ IDEA.
  User: zx
  Date: 2023/4/8
  Time: 21:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Home Page</title>
</head>
<body>
<center>

    <h1>品牌${requestScope.brand}</h1>
    <table border="1">
        <tr>
            <th>商品名称</th>
            <th>商品品牌</th>
            <th>商品价格</th>
            <th>商品详情</th>
        </tr>
        <c:forEach var="good" items="${sessionScope.good}">
            <tr>
                <td>${good.goodsName}</td>
                <td>${good.goodBrand}</td>
                <td>${good.goodsPrice}</td>
                <td>${good.goodsDescription}</td>
            </tr>
        </c:forEach>
    </table>
</center>
</body>
</html>
