package com.example.ec.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * ClassName:Goods
 * Package:com.example.ec.pojo
 * Description:
 *
 * @Author:ZhangXin
 * @Create:2023/4/8-9:46
 * @Version:v1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Goods implements Serializable {
    private Integer goodsId;
    private String goodsName;
    private String goodBrand;
    private String goodsPrice;
    private String goodsDescription;
}
