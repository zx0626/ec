package com.example.ec.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * ClassName:User
 * Package:com.example.ec.pojo
 * Description:
 *
 * @Author:ZhangXin
 * @Create:2023/4/8-9:44
 * @Version:v1.0
 */
@Data

public class User implements Serializable {
    private Integer id;
    private String username;
    private String password;

    private Goods goods;
}
