package com.example.ec.service;

import com.example.ec.dao.GenericDao;
import com.example.ec.pojo.Goods;

import javax.print.DocFlavor;

/**
 * ClassName:GoodsService
 * Package:com.example.ec.service
 * Description:
 *
 * @Author:ZhangXin
 * @Create:2023/4/8-9:59
 * @Version:v1.0
 */
public interface GoodsService extends GenericService<Goods, Integer> {
}
