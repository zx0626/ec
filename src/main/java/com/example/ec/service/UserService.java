package com.example.ec.service;

import com.example.ec.pojo.User;

/**
 * ClassName:UserService
 * Package:com.example.ec.service
 * Description:
 *
 * @Author:ZhangXin
 * @Create:2023/4/8-9:52
 * @Version:v1.0
 */
public interface UserService extends GenericService<User ,Integer>{
}
