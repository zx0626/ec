package com.example.ec.service.impl;

import com.example.ec.dao.GenericDao;

import java.io.Serializable;
import java.util.List;

/**
 * ClassName:GenericServiceImpl
 * Package:com.example.ec.service.impl
 * Description:
 *
 * @Author:ZhangXin
 * @Create:2023/4/8-8:49
 * @Version:v1.0
 */
public abstract class GenericServiceImpl<T extends Serializable,ID extends Number>implements GenericDao<T,ID> {

    GenericDao<T, ID> genericDao;

    abstract void setGenericDao(GenericDao<T, ID> genericDao);

    @Override
    public void create(T t) {
        genericDao.create(t);
    }

    @Override
    public List<T> queryAll() {
        return genericDao.queryAll();
    }

    @Override
    public T queryById(ID id) {
        return genericDao.queryById(id);
    }

    @Override
    public void modify(T t) {
        genericDao.modify(t);
    }

    @Override
    public void remove(ID id) {
        genericDao.remove(id);
    }

    @Override
    public void create(String sqlId, Object parameter) {
        genericDao.create(sqlId, parameter);
    }

    @Override
    public T queryOne(String sqlId, Object parameter) {
        return genericDao.queryOne(sqlId, parameter);
    }

    @Override
    public void modify(String sqlId, Object parameter) {
        genericDao.modify(sqlId, parameter);
    }

    public List<T> queryList(String sqlId, Object parameter) {
        return genericDao.queryList(sqlId, parameter);
    }

    public Object query(String sqlId, Object parameter) {
        return genericDao.query(sqlId, parameter);
    }
}