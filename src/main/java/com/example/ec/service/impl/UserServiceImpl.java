package com.example.ec.service.impl;

import com.example.ec.dao.GenericDao;
import com.example.ec.pojo.User;
import com.example.ec.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * ClassName:UserServiceImpl
 * Package:com.example.ec.service.impl
 * Description:
 *
 * @Author:ZhangXin
 * @Create:2023/4/8-9:52
 * @Version:v1.0
 */
@Service
public class UserServiceImpl extends GenericServiceImpl<User,Integer> implements UserService {
    @Override
    @Autowired
    void setGenericDao(GenericDao<User, Integer> genericDao) {
        super.genericDao=genericDao;
    }
}
