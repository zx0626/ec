package com.example.ec.service.impl;

import com.example.ec.dao.GenericDao;
import com.example.ec.pojo.Goods;
import com.example.ec.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * ClassName:GoodsServiceImpl
 * Package:com.example.ec.service.impl
 * Description:
 *
 * @Author:ZhangXin
 * @Create:2023/4/8-9:59
 * @Version:v1.0
 */
@Service
public class GoodsServiceImpl extends GenericServiceImpl<Goods,Integer>implements GoodsService {
    @Override
    @Autowired
    void setGenericDao(GenericDao<Goods, Integer> genericDao) {
        super.genericDao=genericDao;
    }
}
