package com.example.ec.service;

import java.io.Serializable;
import java.util.List;

/**
 * ClassName:GenericService
 * Package:com.example.ec.service
 * Description:
 *
 * @Author:ZhangXin
 * @Create:2023/4/8-8:49
 * @Version:v1.0
 */
public interface GenericService<T extends Serializable,ID extends Number>{
    void create(T t);

    List<T> queryAll();

    T queryById(ID id);

    void modify(T t);

    void remove(ID id);

    void create(String sqlId , Object parameter);

    T queryOne(String sqlId,Object parameter);

//    T queryById(String sqlId,Object parameter);

    void modify(String sqlId,Object parameter);

    List<T> queryList(String sqlId,Object parameter);

    Object query(String sqlId,Object parameter);
}
