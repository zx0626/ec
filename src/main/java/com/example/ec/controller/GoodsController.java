package com.example.ec.controller;

import com.example.ec.pojo.Goods;
import com.example.ec.service.GoodsService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


/**
 * ClassName:GoodsController
 * Package:com.example.ec.controller
 * Description:
 *
 * @Author:ZhangXin
 * @Create:2023/4/8-9:57
 * @Version:v1.0
 */
@Controller
@RequestMapping("goods")
public class GoodsController extends BaseController {
    private GoodsService goodsServices;

    public GoodsController(GoodsService goodsServices) {
        this.goodsServices = goodsServices;
    }

    @GetMapping("queryById/{id}")
    private String queryById(@PathVariable("id") int goodsId) {
        session.setAttribute("good", goodsServices.queryById(goodsId));
        return "redirect:/admin/edit.jsp";
    }

    @PostMapping("modify")
    private String modify(Goods goods) {
        goodsServices.modify("modifyById", goods);
        session.setAttribute("goods", goodsServices.queryList("queryGoods", new Goods()));
        return "redirect:/admin/index.jsp";

    }


    @GetMapping("removeById/{id}")
    private String remove(@PathVariable("id") int id) {
        goodsServices.remove(id);
        session.setAttribute("goods", goodsServices.queryList("queryGoods", new Goods()));
        return "redirect:/admin/index.jsp";

    }

    @GetMapping("userQueryById/{goodsId}")
    private String userQueryById(@PathVariable("goodsId") int goodsId) {
        Goods goods = goodsServices.queryById(goodsId);
        session.setAttribute("good", goods);
        return "redirect:/edit.jsp";
    }
    @PostMapping("selectByNames")
    private String selectByNames(Goods goods){
        List<Goods> good = goodsServices.queryList("queryByName",goods);
        request.setAttribute("brand",goods.getGoodBrand());
        session.setAttribute("good",good);
        return "redirect:/home.jsp";
    }

    @PostMapping("create")
    private String create(Goods goods){
        goodsServices.create("create",goods);
        session.setAttribute("goods", goodsServices.queryList("queryGoods", new Goods()));
        return "redirect:/admin/index.jsp";
    }
    @GetMapping("signOut")
    private String signOut(){
        session.invalidate();
        return "/index.jsp";
    }
//    @GetMapping("signOut1")
//    private String signOut1(){
//        session.setAttribute("goods", goodsServices.queryList("queryGoods", new Goods()));
//        return "redirect:/admin/index.jsp";
//    }

}
