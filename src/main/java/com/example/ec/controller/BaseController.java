package com.example.ec.controller;

import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * ClassName:BaseController
 * Package:com.example.ssmsample.controller
 * Description:
 *
 * @Author:ZhangXin
 * @Create:2023/3/31-11:42
 * @Version:v1.0
 */
public class BaseController {
    HttpServletRequest request;
    HttpServletResponse response;
    HttpSession session;
    ServletContext application;


    @ModelAttribute
    private  void set(HttpServletRequest request,HttpServletResponse response){
        this.request = request;
        this.response = response;
        this.session =this.request.getSession();
        this.application = this.request.getServletContext();
    }
}
