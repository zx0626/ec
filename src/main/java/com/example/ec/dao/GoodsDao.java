package com.example.ec.dao;

import com.example.ec.pojo.Goods;

/**
 * ClassName:GoodsDao
 * Package:com.example.ec.dao
 * Description:
 *
 * @Author:ZhangXin
 * @Create:2023/4/8-9:58
 * @Version:v1.0
 */
public interface GoodsDao extends GenericDao<Goods,Integer>{
}
