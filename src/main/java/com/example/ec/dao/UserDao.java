package com.example.ec.dao;

import com.example.ec.pojo.User;

/**
 * ClassName:UserDao
 * Package:com.example.ec.dao
 * Description:
 *
 * @Author:ZhangXin
 * @Create:2023/4/8-9:49
 * @Version:v1.0
 */
public interface UserDao extends GenericDao<User,Integer>{
}
