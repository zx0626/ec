package com.example.ec.dao;

import java.io.Serializable;
import java.util.List;

/**
 * ClassName:GenericDao
 * Package:com.example.ec.dao
 * Description:
 *
 * @Author:ZhangXin
 * @Create:2023/4/8-8:48
 * @Version:v1.0
 */
public interface GenericDao <T extends Serializable,ID extends Number>{

    void create(T t);

    List<T> queryAll();//查询

    T queryById(ID id);

    void modify(T t);

    void remove(ID id);

    void create(String sqlId ,Object parameter);

    T queryOne(String sqlId, Object parameter);//查询

    void modify(String sqlId, Object parameter);

    List<T> queryList(String sqlId, Object parameter);//查询

    Object query(String sqlId, Object parameter);
}
