package com.example.ec.dao.impl;

import com.example.ec.dao.GenericDao;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * ClassName:GenericDaoImpl
 * Package:com.example.ec.dao.impl
 * Description:
 *
 * @Author:ZhangXin
 * @Create:2023/4/8-8:48
 * @Version:v1.0
 */
public class GenericDaoImpl <T extends Serializable,ID extends Number>implements GenericDao<T ,ID> {

    private SqlSession sqlSession;

    @Autowired
    public void setSqlSession(SqlSession sqlSession) {
        this.sqlSession = sqlSession;
    }

    private String namespace; // 命名空间

    @SuppressWarnings("unchecked")
    GenericDaoImpl() {
        ParameterizedType parameterizedType = (ParameterizedType) getClass().getGenericSuperclass();
        Class<T> clazz = (Class<T>) parameterizedType.getActualTypeArguments()[0];
        this.namespace = StringUtils.uncapitalize(clazz.getSimpleName());
    }


    private String getStatement(String sqlId) {
        return this.namespace.concat(".").concat(sqlId);
    }


    @Override
    public void create(T t) {
        sqlSession.insert(getStatement("create"), t);
    }

    @Override
    public List<T> queryAll() {
        return sqlSession.selectList(getStatement("queryAll"));
    }

    @Override
    public T queryById(ID id) {
        return sqlSession.selectOne(getStatement("queryById"), id);
    }

    @Override
    public void modify(T t) {
        sqlSession.update(getStatement("modify"), t);
    }

    @Override
    public void remove(ID id) {
        sqlSession.delete(getStatement("remove"), id);
    }

    @Override
    public void create(String sqlId, Object parameter) {
        sqlSession.insert(getStatement(sqlId), parameter);
    }

    @Override
    public T queryOne(String sqlId, Object parameter) {
        return sqlSession.selectOne(getStatement(sqlId), parameter);
    }

    @Override
    public void modify(String sqlId, Object parameter) {
        sqlSession.update(getStatement(sqlId), parameter);
    }

    @Override
    public List<T> queryList(String sqlId, Object parameter) {
        return sqlSession.selectList(getStatement(sqlId), parameter);
    }

    @Override
    public Object query(String sqlId, Object parameter) {
        return sqlSession.selectOne(getStatement(sqlId), parameter);
    }
}