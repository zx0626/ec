package com.example.ec.dao.impl;

import com.example.ec.dao.UserDao;
import com.example.ec.pojo.User;
import org.springframework.stereotype.Repository;

/**
 * ClassName:UserDaoImpl
 * Package:com.example.ec.dao.impl
 * Description:
 *
 * @Author:ZhangXin
 * @Create:2023/4/8-9:49
 * @Version:v1.0
 */
@Repository
public class UserDaoImpl extends GenericDaoImpl<User,Integer> implements UserDao {
}
