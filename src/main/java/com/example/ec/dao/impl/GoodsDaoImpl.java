package com.example.ec.dao.impl;

import com.example.ec.dao.GenericDao;
import com.example.ec.dao.GoodsDao;
import com.example.ec.pojo.Goods;
import org.springframework.stereotype.Repository;

/**
 * ClassName:GoodsDaoIMPL
 * Package:com.example.ec.dao.impl
 * Description:
 *
 * @Author:ZhangXin
 * @Create:2023/4/8-9:57
 * @Version:v1.0
 */
@Repository
public class GoodsDaoImpl extends GenericDaoImpl<Goods,Integer>implements GoodsDao {
}
